#pragma once
#include <string>
#include <stdio.h>
#include "cTexture.h"
#include "Constants.h"


class cScene
{
public:
	cScene(void);
	virtual ~cScene(void);

	bool LoadLevel(int level);
	void Draw(int tex_id);
	int *GetMap();
	void setOffset(int value);
	int getOffset();

	bool detectCollision(int x, int y);
	bool checkFinal(int x, int y, int level);

private:
	int map[SCENE_WIDTH * SCENE_HEIGHT];	//scene
	int id_DL;								//actual level display list
	int offset;
};
