#include "cGame.h"
#include "Globals.h"


cGame::cGame(void)
{
	Menu = cMenu();
	this->multiplayer = false;
	godmode = false;
}

cGame::~cGame(void)
{
}

bool cGame::Init(int lvl, int estado)
{
	srand(time(NULL));
	bool res=true;
	gOMovement = 0.f;
	gOPosition = 0;
	lcMovement = 0.f;

	state = estado;
	currentlevel = lvl;
	
	//Graphics initialization
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,GAME_WIDTH,0,GAME_HEIGHT,0,1);
	glMatrixMode(GL_MODELVIEW);
	
	glAlphaFunc(GL_GREATER, 0.05f);
	glEnable(GL_ALPHA_TEST);

	//Load textures
	if (lvl == 1) res = Data.LoadImage(IMG_BLOCKS,"Resources/Maps/w1.png",GL_RGBA);
	else if (lvl == 2) res = Data.LoadImage(IMG_BLOCKS,"Resources/Maps/w3.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_PLAYER,"Resources/Sprites/mercenary.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_TITLE_TEXT,"Resources/Fonts/menuTitle.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_MENU_TEXT,"Resources/Fonts/menu.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_HELP,"Resources/Fonts/help.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_HELP,"Resources/Fonts/background.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_CREDITS,"Resources/Fonts/credits.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_BULLETS1,"Resources/Sprites/bullets.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_SCORE,"Resources/Fonts/score.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_BOMB_INFO,"Resources/Sprites/bomb.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_ENEMY1,"Resources/Sprites/Enemy1.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_ENEMY2,"Resources/Sprites/Enemy2.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_PLAYER2,"Resources/Sprites/Player2.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_BOMB,"Resources/Sprites/Explosions.png",GL_RGBA);
	if(!res) return false;

	
	//Scene initialization
	res = Scene.LoadLevel(currentlevel);
	if(!res) return false;

	//Player initialization
	Player.Initialization1();
	
	//Player2 initialization
	Player2.Initialization2();

	InitializeEnemies();

	this->dictionaryCoord = new map<char, maxmin>();
	utils::calculateDictionaryTextBoundaries(4,10, this->dictionaryCoord);

	//Menu
	res = Menu.init();
	if(!res) return false;

	return res;
}

bool cGame::Loop()
{
	int t1 = glutGet(GLUT_ELAPSED_TIME);


	bool res = Process();
	if(!res) return res;

	Render();
	
	int t2 = 0;
	while ((t2-t1) < GAME_FPS) t2 = glutGet(GLUT_ELAPSED_TIME);
	return res;
}

void cGame::Finalize()
{
	exit(0);
}

//Input
void cGame::ReadKeyboard(unsigned char key, int x, int y, bool press)
{
	keys[key] = press;
}

void cGame::ReadKeyboardSpecial(unsigned char key, int x, int y, bool press)
{
	sKeys[key] = press;
}

void cGame::ReadMouse(int button, int state, int x, int y)
{
}

//Process
bool cGame::Process() 
{
													/*Variable setting*/
	bool res=true;
	int x1, y1, x2, y2;
	Player.GetPosition(&x1, &y1);
	if (multiplayer) Player2.GetPosition(&x2, &y2);

	//Keys mapping
	playerkeys kp1 = {'a','d','w','s',32,'q'};
	playerkeys kp2 = {GLUT_KEY_LEFT,GLUT_KEY_RIGHT, GLUT_KEY_UP,GLUT_KEY_DOWN,'-',KEYBOARD_KEY_INTRO};
	
        
	if(state == PLAY ) { 
		// No hi ha menu, juguem
													/* Player input */
		
		//Menu in-game
		if(sKeys[KEYBOARD_KEY_ESC]) {
			state = MENU_ING;
			Menu.modify(MENU_ING, 0);
			sKeys[KEYBOARD_KEY_ESC] = false;
			return res;
		}

		//God Mode
		if (keys['g']) switchGodMode();
		
		//Bloqueo de c�mara en multijugador
		if(multiplayer) {			
			Player.setCameraLock(y2);				
			Player2.setCameraLock(y1);
		}

        //Movimiento y disparos del jugador 1
		if (!Player.getCameraLock()) {
			Player.readKeyboard(kp1, Scene.GetMap(), &Bullets, keys, sKeys, &Bombs);
		}
		//Bombas del jugador 1
		if ((keys[kp1.bomb] && Player.getBombs() > 0) || Player.getBombarounds() < 30 && Player.getBombarounds() > 0) {
			Player.ShootBomb(&Bombs);
			Player.setBombs(0);
		}
		
		if(multiplayer) {
			//Movimiento y disparos del jugador 2
			if (!Player2.getCameraLock()) {
				Player2.readKeyboard(kp2, Scene.GetMap(), &Bullets, sKeys, keys, &Bombs);
			}
			//Bombas del jugador 2
			if ((sKeys[kp2.bomb] && Player2.getBombs() > 0) || Player2.getBombarounds() < 30 && Player2.getBombarounds() > 0) {
					Player2.ShootBomb(&Bombs);
					Player2.setBombs(0);
			}
		}

														/*Game Logic*/
		/*Antes de este punto, las posiciones de los jugadores pueden haber sido modificadas*/
		Player.GetPosition(&x1, &y1);
		Player2.GetPosition(&x2, &y2);
		
		//Logica player 1
        Player.Logic();
		//Logica player 2
		if(multiplayer) Player2.Logic();

		//Enemies
		for (list<cEnemy>::iterator it = Enemies.begin(); it != Enemies.end();) {
			//Aplicar IA de los enemigos
			int rnd;
			if (multiplayer) rnd = rand()%2;
			else rnd = 0;
			if (rnd == 0)				it->EnemyAI(Scene.GetMap(),x1,y1,&Bullets);
			else if (rnd == 1)			it->EnemyAI(Scene.GetMap(),x2,y2,&Bullets);

			//Bombs collisions
			if (it->CollideBombs(x1, y1, Player.getBombarounds())) Player.addScore(1000);
			if (multiplayer) {
				if (it->CollideBombs(x2, y2, Player2.getBombarounds())) Player2.addScore(1000);
			}

			//Death logic
			if (it->getLives() <= 0) it = Enemies.erase(it); 
			else it++;
		}

		//Movimiento de las balas + Colisiones entre Bichos y Bullets
		for (list<cShoot>::iterator it = Bullets.begin(); it != Bullets.end(); ) {
			
			if (it->getDistance() < 0) it = Bullets.erase(it);			//Alcance de las balas
			else {
				int x,y,w,h,idsh;
				it->Move(Scene.GetMap());	//Mueve la bala a donde le pertoca
				it->getPosition(&x, &y);	//Posicion bala
				it->getWidthHeight(&w,&h);	//Tama�o de la bala
				idsh = it->getIdShooter();	//Quien la ha disparado

				if (it->getState() != STATE_BDESTROYED) {

					//Comprueba colisiones con player 1 y 2
					if (Player.isShot(idsh,x,y,w,h)) {
						it->CollidesPlayer();  //Bala pasa a colisionar
						if(Player.getLives() > 0 && !godmode) Player.decreaseLives();
					}
					if (multiplayer && Player2.isShot(idsh,x,y,w,h)) {
						it->CollidesPlayer();  //Bala pasa a colisionar
						if(Player2.getLives() > 0 && !godmode) Player2.decreaseLives();
					}

					//Logica muerte
					if (Player.getLives() == 0 || Player2.getLives() == 0) state = GAME_OVER;
				}

				//Funcion para mirar colisiones de balas con enemigos
				//(Se comprueba por cada par de enemigo y bala, por eso el for esta embebido en el de balas)
				for (list<cEnemy>::iterator it2 = Enemies.begin(); it2 != Enemies.end(); ) {
					if (it->getState() != 9 && it2->isShot(idsh,x,y,w,h)) { //it state 9 == bala en estado destruido
						it->CollidesPlayer();
						it2->decreaseLives();
					}
					if (it2->getLives() <= 0) {
						if (idsh == 1) Player.addScore(1000);
						else if (idsh == 2) Player2.addScore(1000);
						it2 = Enemies.erase(it2); 
					}
					else it2++;
				}
				it++;
			}
		}

		//Bombas
		for (list<cBomb>::iterator it = Bombs.begin(); it != Bombs.end();) {
			if (it->isEnded()) it = Bombs.erase(it);
			else it++;
		}

		//Chequear final del mapa
		if (Scene.checkFinal(x1, y1, currentlevel)) state = CHANGING_LEVEL; 

	}
	else if(state == MENU || state == MENU_ING){			//state == MENU
		if(sKeys[GLUT_KEY_UP]) Menu.keyPress(GLUT_KEY_UP);
		else if(sKeys[GLUT_KEY_DOWN]) Menu.keyPress(GLUT_KEY_DOWN);
		else if(sKeys[KEYBOARD_KEY_INTRO] ) {
			int ps = Menu.keyPress(KEYBOARD_KEY_INTRO);
			switch (ps)
			{
			case MULTIPLAYER:
				multiplayer = true;
				state = PLAY;
				break;
			case PLAY:
				state = PLAY;
				break;
			case EXIT:
				Finalize(); break;
			case MENU:
				state = MENU; break;
			case EXIT_TO_MMENU:
				ResetGame(1,MENU);
				multiplayer = false;
				break;
			default: 
				if(ps != -1) state = ps;
				break;
			}
			
		}
		sKeys[GLUT_KEY_UP] = sKeys[GLUT_KEY_DOWN] = sKeys[KEYBOARD_KEY_INTRO] = sKeys[KEYBOARD_KEY_ESC] = false;
	}
	else {
		if(sKeys[KEYBOARD_KEY_ESC] && (state == HELP || state == CREDITS)) state = Menu.returnPreviousMenu();
		else if (sKeys[KEYBOARD_KEY_INTRO] && state == GAME_OVER) {
			ResetGame(currentlevel,MENU);
			//state = MENU;
			gOMovement = 0.f;
			gOPosition = 0;
		}
		else if (sKeys[KEYBOARD_KEY_INTRO] && state == CREDITS)ResetGame(1,MENU);
		
		sKeys[GLUT_KEY_UP] = sKeys[GLUT_KEY_DOWN] = sKeys[KEYBOARD_KEY_INTRO] = sKeys[KEYBOARD_KEY_ESC] = false;
	}
    return res;
}

//Output
void cGame::Render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	if(state == GAME_OVER) {
		string t = "game over";
		char c;
		if(gOPosition < 9) c = t[gOPosition];
		else c = '*';
		utils::renderGameOver(t.substr(0,gOPosition),c,gOMovement,dictionaryCoord);
		
		float limit = 160.0f;
		if(gOPosition > 4) limit += 30;
		
		if(gOMovement >= limit && gOPosition < 9) {
			++gOPosition;
			if(gOPosition == 4) ++gOPosition;
			gOMovement = 0;
		}
		else if(gOMovement < limit) gOMovement += 5.0f;
		
	}
	else if(state == CHANGING_LEVEL) {
		if(lcMovement < 800) {
			if(multiplayer) utils::renderLevelComplete(currentlevel,Player.getScore() + Player2.getScore(),dictionaryCoord, lcMovement);
			else utils::renderLevelComplete(currentlevel,Player.getScore(),dictionaryCoord, lcMovement);
			lcMovement += 7;
		}
		else {
			lcMovement = 0;
			if(currentlevel == 1) ResetGame(2, PLAY);	
			else if (currentlevel == 2) {
				Menu.previousMenu = MENU;
				ResetGame(1, CREDITS);
			}
		}
	}
	else if(state == PLAY) {

		//Update Scene and Players
		Scene.Draw(Data.GetID(IMG_BLOCKS));
		Player.Draw(Data.GetID(IMG_PLAYER));
		int plx, ply;
		Player.GetPosition(&plx, &ply);
		if(multiplayer) {
			int pl2x, pl2y;
			Player2.Draw(Data.GetID(IMG_PLAYER2));
			Player2.GetPosition(&pl2x, &pl2y);
			ply = (ply+pl2y)/2;
		}
		Scene.setOffset(ply);
		
		//Enemies
		for (list<cEnemy>::iterator it = Enemies.begin(); it != Enemies.end(); ++it) {
			if (it->getKind() == 1) it->Draw(Data.GetID(IMG_ENEMY1));
			else if (it->getKind() == 2) it->Draw(Data.GetID(IMG_ENEMY2));
		}

		//Bullets
		for (list<cShoot>::iterator it = Bullets.begin(); it != Bullets.end(); ++it) it->Draw(Data.GetID(IMG_BULLETS1));

		//Bombs
		for (list<cBomb>::iterator it = Bombs.begin(); it != Bombs.end(); ++it) it->Draw(Data.GetID(IMG_BOMB));

		//Players screen info
		utils::renderPlayerInfo(1,Player.getScore(),Player.getLives(), Player.getBombs(), dictionaryCoord, Scene.getOffset());
		if(multiplayer) utils::renderPlayerInfo(2,Player2.getScore(),Player2.getLives(), Player2.getBombs(), dictionaryCoord, Scene.getOffset());
	}
	else if(state == HELP) utils::renderHelp();
	else if(state == CREDITS) {
		utils::renderCredits();
		Menu.renderTitle(dictionaryCoord);
	}
	else Menu.render(dictionaryCoord);

	glutSwapBuffers();
}

void cGame::ResetGame(int lvl, int estado) {
	Bullets.clear();
	Enemies.clear();
	Bombs.clear();
	Init(lvl,estado);
	
}

void cGame::switchGodMode()
{
	if (!godmode) godmode = true;
	else godmode = false;
}

bool cGame::getGodMode() {
	return godmode;
}

void cGame::InitializeEnemies() {
	int posx, posy;
	for (int i = 1; i <= 25; ++i) {
		
		if (i <= 20) {					//ENEMIES DE TIPO 1
			int posx = (((rand()%(SCENE_WIDTH*TILE_SIZE))/TILE_SIZE)*TILE_SIZE); //El )/TILE_SIZE)*TILE_SIZE) es para que
			int posy = (((rand()%(SCENE_HEIGHT*TILE_SIZE))/TILE_SIZE)*TILE_SIZE); //Las posiciones que pruebe concuerden con tiles
			while(Scene.detectCollision(posx/TILE_SIZE,posy/TILE_SIZE)) {
				posx = (((rand()%(SCENE_WIDTH*TILE_SIZE))/TILE_SIZE)*TILE_SIZE);
				posy = (((rand()%(SCENE_HEIGHT*TILE_SIZE))/TILE_SIZE)*TILE_SIZE);
			}
			cEnemy enemy(posx,posy,32,32,1);
			enemy.SetState(STATE_LOOKBOTTOM);
			enemy.setId((i+3)*-1);
			Enemies.push_back(enemy);
		}
		else {							//ENEMIES DE TIPO 2
			if (currentlevel == 1) {
				if (i == 21){		posx = 19*TILE_SIZE;		posy = 26*TILE_SIZE;}
				else if (i == 22){	posx = 19*TILE_SIZE;		posy = 57*TILE_SIZE;}
				else if (i == 23){	posx = 8*TILE_SIZE;			posy = 86*TILE_SIZE;}
				else if (i == 24){	posx = 33*TILE_SIZE;		posy = 86*TILE_SIZE;}
				else if (i == 25){	posx = 19*TILE_SIZE;		posy = 146*TILE_SIZE;}
			}
			else if (currentlevel == 2) {
				if (i == 21){		posx = 19*TILE_SIZE;		posy = 26*TILE_SIZE;}
				else if (i == 22){	posx = 35*TILE_SIZE;		posy = 61*TILE_SIZE;}
				else if (i == 23){	posx = 3*TILE_SIZE;			posy = 36*TILE_SIZE;}
				else if (i == 24){	posx = 18*TILE_SIZE;		posy = 136*TILE_SIZE;}
				else if (i == 25){	posx = 18*TILE_SIZE;		posy = 174*TILE_SIZE;}
			}

			cEnemy enemy(posx,posy,32,32,2);
			enemy.SetState(STATE_LOOKBOTTOM);
			enemy.setId((i+3)*-1);
			Enemies.push_back(enemy);
		}
	}
}