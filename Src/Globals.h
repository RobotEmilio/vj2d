
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gl/glut.h>
#include <iostream>
#include <list>
#include <ctime>
#include <queue>
using namespace std;

#ifndef PLAYER_KEYS
#define PLAYER_KEYS

struct playerkeys{
    unsigned char left, right, up, down, shoot, bomb;
};
#endif