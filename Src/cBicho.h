#pragma once

#include "cTexture.h"
#include "Globals.h"
#include "Constants.h"
#include "cShoot.h"
#include "cBomb.h"


class cRect
{
public:
	int left,top,
		right,bottom;
};

class cBicho
{
public:
	cBicho(void);
	cBicho(int x,int y,int w,int h);
	~cBicho(void);

	void SetPosition(int x,int y);
	void GetPosition(int *x,int *y);
	void SetTile(int tx,int ty);
	void GetTile(int *tx,int *ty);
	void SetWidthHeight(int w,int h);
	void GetWidthHeight(int *w,int *h);

	bool Collides(int *map);
	bool CollidesBoundaries(int *map);
	bool CollidesStatic(int *map);
	void GetArea(cRect *rc);
	void DrawRect(int tex_id,float xo,float yo,float xf,float yf);


	void MoveRight(int *map);
	void MoveLeft(int *map);
	void MoveTop(int *map);
	void MoveBottom(int *map);
	void MoveTopLeft(int *map);
	void MoveBottomLeft(int *map);
	void MoveTopRight(int *map);
	void MoveBottomRight(int *map);
	void Stop();

	cShoot Shoot();
	bool isShot(int idbullet, int bx, int by, int bw, int bh);
	void ShootBomb(list<cBomb> * Bombs);

	int  GetState();
	void SetState(int s);

	void NextFrame(int max);
	int  GetFrame();
	
	bool isShooting();
	void notShooting();

	int getId();
	void setId(int i);

	int getBombarounds();
	void ResetBombarounds();

protected:

	int id;		//Positivo si jugador 1 o 2, negativo si enemigo
	int x,y;	//Posicion
	int w,h;	//Anchura

	int state;	//Estado del bicho

	bool shooting;		//Esta disparando?
	int shootingdelay;	//Retraso de disparo
	int bombarounds;
	int damage_invencibility;

	int seq,delay;	
};
