﻿#include "Utils.h"

 utils:: utils()
{
}

 utils::~ utils()
{
}

void utils::calculateDictionaryTextBoundaries(int rows, int cols,  map<char, maxmin> *dictMap) 
{
        maxmin arr = {0.f,0.f,1.f/cols, 1.f/rows};
        //Construim les coordenades de la textura per accés rŕpid
        string abc = "abcdefghijklmnopqrstuvwxyz[*]-0123456789";
        float sumx, sumy;
        sumx = sumy = 0.f;
        for(char& c : abc) {
                if(sumx >= 1.f) {
                        sumy += 1.f/rows;
                        sumx = 0.f;
                }
                arr.xmin = sumx;
                arr.ymin = sumy;
                arr.xmax = sumx + 0.1f;
                arr.ymax = sumy + 0.25f;
                (* dictMap)[c] = arr;
                sumx += 0.1f;
        }
}

void utils::renderHelp()
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,IMG_HELP);
	utils::paintBackgroundQuad();
}

void utils::renderBackground()
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,IMG_BACKGROUND);
	utils::paintBackgroundQuad();
}

void utils::paintBackgroundQuad()
{
	glBegin(GL_QUADS);	
			glTexCoord2f(0.f,0.f);	
			glVertex2i(0,GAME_HEIGHT);
			glTexCoord2f(0.f,1.f);	
			glVertex2i(0,0);
			glTexCoord2f(1.f,1.f);	
			glVertex2i(GAME_WIDTH,0);
			glTexCoord2f(1.f,0.f);	
			glVertex2i(GAME_WIDTH,GAME_HEIGHT);
	glEnd();
}

void utils::renderCredits()
{
	utils::renderBackground();
	glBindTexture(GL_TEXTURE_2D,IMG_CREDITS);
	utils::paintBackgroundQuad();
}

void utils::renderPlayerInfo(int player, int score, int lives, int bombs, map<char, maxmin> *dictMap, int offset)
{	
	//converting ints to strings
	ostringstream convert;
		//lives
	convert << lives;
	string livesS = convert.str();
		//bombs
	convert.clear(); convert.str("");
	convert << bombs;
	string bombsS = convert.str();
		//score
	convert.clear(); convert.str("");
	convert.width(5);
	convert << score;
	string scoreS = convert.str();

	int sumy = 0;
	int sumx = 0;
	int iniPlayerX = INI_X_INFO_L_P1;
	string output = "*";
	output.append(livesS);

	//Player 1 or 2?
	if (player == 2) iniPlayerX = INI_X_INFO_L_P2;

	//Print lives left
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,IMG_SCORE);
	glPushMatrix();
	if(offset > 0)glTranslatef(0.0f, offset, 0.0f);
	glBegin(GL_QUADS);
	for(char& c : output) {
			maxmin font = (*dictMap)[c];
			glTexCoord2f(font.xmin, font.ymin);	
			glVertex2i(iniPlayerX+sumx+CHAR_SEPARATION_HOR,INI_Y_INFO_PLAYER-sumy);
			glTexCoord2f(font.xmin, font.ymax);	
			glVertex2i(iniPlayerX+sumx+CHAR_SEPARATION_HOR,INI_Y_INFO_PLAYER-INFO_LETTER_HEIGHT-sumy);
			glTexCoord2f(font.xmax, font.ymax);	
			glVertex2i(iniPlayerX+INFO_LETTER_WIDTH+sumx,INI_Y_INFO_PLAYER-INFO_LETTER_HEIGHT-sumy);
			glTexCoord2f(font.xmax,font.ymin);	
			glVertex2i(iniPlayerX+INFO_LETTER_WIDTH+sumx,INI_Y_INFO_PLAYER-sumy);
			sumx += INFO_LETTER_WIDTH;
		}
	glEnd();
	glPopMatrix();

	//Player 1 or 2?
	if(player == 1) {
		iniPlayerX = INI_X_INFO_P1;
		output = "player1<";
	}	
	else if (player == 2) {
		iniPlayerX = INI_X_INFO_P2;
		output = "player2<";
	}

	//Print name and score
	sumx = sumy = 0;
	output = output.append(scoreS);
	glPushMatrix();
	if(offset > 0) glTranslatef(0.0f, offset, 0.0f);
	glBegin(GL_QUADS);
		for(char& c : output) {
			if(c == '<') {
				sumy += INFO_LETTER_HEIGHT;
				sumx = 0;
			}
			maxmin font = (*dictMap)[c];	

			glTexCoord2f(font.xmin, font.ymin);	
			glVertex2i(iniPlayerX+sumx+CHAR_SEPARATION_HOR,INI_Y_INFO_PLAYER-sumy);
			glTexCoord2f(font.xmin, font.ymax);	
			glVertex2i(iniPlayerX+sumx+CHAR_SEPARATION_HOR,INI_Y_INFO_PLAYER-INFO_LETTER_HEIGHT-sumy);
			glTexCoord2f(font.xmax, font.ymax);	
			glVertex2i(iniPlayerX+INFO_LETTER_WIDTH+sumx,INI_Y_INFO_PLAYER-INFO_LETTER_HEIGHT-sumy);
			glTexCoord2f(font.xmax,font.ymin);	
			glVertex2i(iniPlayerX+INFO_LETTER_WIDTH+sumx,INI_Y_INFO_PLAYER-sumy);
			sumx += INFO_LETTER_WIDTH;
		}
	glEnd();
	glPopMatrix();

	//Print bombs left
	//Player 1 or 2?
	iniPlayerX = INFO_BOMBS_X;
	if (player == 2) iniPlayerX = INFO_BOMBS_X_P2;

	glPushMatrix();
	if(offset > 0) glTranslatef(0.0f, offset, 0.0f);
	glBegin(GL_QUADS);
	//Number bombs left
	for(char& c : bombsS) {
		maxmin font = (*dictMap)[c];	
		glTexCoord2f(font.xmin, font.ymin);	
		glVertex2i(iniPlayerX+INFO_BOMBS_WIDTH+CHAR_SEPARATION_HOR,INFO_BOMBS_Y);
		glTexCoord2f(font.xmin, font.ymax);	
		glVertex2i(iniPlayerX+INFO_BOMBS_WIDTH+CHAR_SEPARATION_HOR,INFO_BOMBS_Y-INFO_BOMBS_WIDTH);
		glTexCoord2f(font.xmax, font.ymax);	
		glVertex2i(iniPlayerX+INFO_BOMBS_WIDTH*2+CHAR_SEPARATION_HOR,INFO_BOMBS_Y-INFO_BOMBS_WIDTH);
		glTexCoord2f(font.xmax,font.ymin);	
		glVertex2i(iniPlayerX+INFO_BOMBS_WIDTH*2+CHAR_SEPARATION_HOR,INFO_BOMBS_Y);
	}
	glEnd();

	//Bomb icon
	glBindTexture(GL_TEXTURE_2D,IMG_BOMB_INFO);
	//if(offset > 0) glTranslatef(0.0f, offset, 0.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(0.f, 0.f);	
	glVertex2i(iniPlayerX,INFO_BOMBS_Y);
	glTexCoord2f(0.f, 1.f);	
	glVertex2i(iniPlayerX,INFO_BOMBS_Y-INFO_BOMBS_WIDTH);
	glTexCoord2f(1.f, 1.f);	
	glVertex2i(iniPlayerX+INFO_BOMBS_WIDTH,INFO_BOMBS_Y-INFO_BOMBS_WIDTH);
	glTexCoord2f(1.f,0.f);	
	glVertex2i(iniPlayerX+INFO_BOMBS_WIDTH,INFO_BOMBS_Y);
	
	glEnd();
	glPopMatrix();
}

int utils::paintFixLetters(string fixedLetters, map<char, maxmin> *dictMap, int textureID)
{
	glBindTexture(GL_TEXTURE_2D,textureID);
	int sumx, sumy;
	glBegin(GL_QUADS);
		sumx = sumy = 0;
		for(char& c : fixedLetters) {
			if(c == ' ') {
				sumy += 30;
				sumx += 30;
			}
			else {
				maxmin font = (*dictMap)[c];		
				glTexCoord2f(font.xmin, font.ymin);	
				glVertex2i(INI_TITLE_GO_X+sumx+CHAR_SEPARATION_HOR,INI_TITLE_GO_Y-sumy);
				glTexCoord2f(font.xmin, font.ymax);	
				glVertex2i(INI_TITLE_GO_X+sumx+CHAR_SEPARATION_HOR,INI_TITLE_GO_Y-TITLE_LETTER_HEIGHT-sumy);
				glTexCoord2f(font.xmax, font.ymax);	
				glVertex2i(INI_TITLE_GO_X+TITLE_LETTER_WIDTH+sumx,INI_TITLE_GO_Y-TITLE_LETTER_HEIGHT-sumy);
				glTexCoord2f(font.xmax,font.ymin);	
				glVertex2i(INI_TITLE_GO_X+TITLE_LETTER_WIDTH+sumx,INI_TITLE_GO_Y-sumy);
				sumx += TITLE_LETTER_WIDTH;
			}
		}
	glEnd();
	return sumx;
}

void utils::paintMovementLetter(char movementLetter, float movement, int sumx,map<char, maxmin> *dictMap,int textureID) 
{
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D,textureID);
	glTranslatef(0,-movement,0);
	glBegin(GL_QUADS);
		maxmin font = (*dictMap)[movementLetter];
		glTexCoord2f(font.xmin, font.ymin);	
		glVertex2i(INI_TITLE_GO_X+sumx+CHAR_SEPARATION_HOR,480);
		glTexCoord2f(font.xmin, font.ymax);	
		glVertex2i(INI_TITLE_GO_X+sumx+CHAR_SEPARATION_HOR,480-TITLE_LETTER_HEIGHT);
		glTexCoord2f(font.xmax, font.ymax);	
		glVertex2i(INI_TITLE_GO_X+TITLE_LETTER_WIDTH+sumx,480-TITLE_LETTER_HEIGHT);
		glTexCoord2f(font.xmax,font.ymin);	
		glVertex2i(INI_TITLE_GO_X+TITLE_LETTER_WIDTH+sumx,480);
	glEnd();
	glPopMatrix();
}

void utils::paintSubtitle(string subtitle, map<char, maxmin> *dictMap,int textureID)
{
	glBindTexture(GL_TEXTURE_2D,textureID);
	glBegin(GL_QUADS);
		int sumx = 0;
		for(char& c : subtitle) {
			if(c == ' ') sumx += 10;
			else {
				maxmin font = (*dictMap)[c];		
				glTexCoord2f(font.xmin, font.ymin);	
				glVertex2i(INI_SUBTITLE_GO_X+sumx+CHAR_SEPARATION_HOR,INI_SUBTITLE_GO_Y);
				glTexCoord2f(font.xmin, font.ymax);	
				glVertex2i(INI_SUBTITLE_GO_X+sumx+CHAR_SEPARATION_HOR,INI_SUBTITLE_GO_Y-SUBTITLE_LETTER_H);
				glTexCoord2f(font.xmax, font.ymax);	
				glVertex2i(INI_SUBTITLE_GO_X+SUBTITLE_LETTER_W+sumx,INI_SUBTITLE_GO_Y-SUBTITLE_LETTER_H);
				glTexCoord2f(font.xmax,font.ymin);	
				glVertex2i(INI_SUBTITLE_GO_X+SUBTITLE_LETTER_W+sumx,INI_SUBTITLE_GO_Y);
				sumx += SUBTITLE_LETTER_W;
			}
		}
	glEnd();
}

 void utils::renderGameOver(string fixedLetters, char movementLetter,float movement, map<char, maxmin> *dictMap)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,IMG_BACKGROUND);
	utils::paintBackgroundQuad();
	int sumx = utils::paintFixLetters(fixedLetters, dictMap, IMG_MENU_TEXT);
	if(movementLetter != '*') utils::paintMovementLetter(movementLetter, movement, sumx,dictMap, IMG_MENU_TEXT);
	else utils::paintSubtitle("[press enter to continue]", dictMap, IMG_SCORE);
}

 void utils::renderLevelComplete(int level, double score, map<char, maxmin> *dictMap, float trans)
 {
	string scoreS =  "score:_";
	string levelS =  "level_";
	ostringstream convert;
	convert << score;
	scoreS = scoreS.append(convert.str());
	convert.clear(); convert.str("");
	convert << level;
	levelS = levelS.append(convert.str());
	levelS = levelS.append("_complete");

	if(trans < 480)glTranslatef(480-trans,0,0),
	//Level X
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,IMG_BACKGROUND);
	utils::paintBackgroundQuad();
	glPushMatrix();
	glTranslatef(50.f,150.f,0);
	glScalef(0.5,0.5,1);
	int sumx = utils::paintFixLetters(levelS, dictMap, IMG_MENU_TEXT);
	glPopMatrix();
	//score
	glPushMatrix();
	glTranslatef(150.0f,100.0f,0);
	utils::paintSubtitle(scoreS, dictMap, IMG_SCORE);
	glPopMatrix();
 }