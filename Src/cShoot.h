#pragma once

#include "Constants.h"
#include "Globals.h"

//Tipos de proyectiles
#define BULLET	1
#define	BOMB	2

//Da�o de proyectil
#define DBULLET	1

class cShoot
{

private:

	//Metadata
	int state;
	int type;
	int damage;
	bool hit;
	//Distancia
	int distance;
	//Posicion actual
	int x,y;
	//Direcci�n
	double dx, dy;
	// Width y height
	int w,h;
	//ShooterId (para identificar si la bala es amiga o enemiga)
	int idshooter;

public:
	cShoot(void);
	cShoot(cShoot * c);
	cShoot(int shooter, int type, int state, int a, int b);
	~cShoot(void);

	int getDistance();
	void setDistance(int a);

	void getPosition(int *a, int *b);
	void setPosition(int a, int b);

	void getDirection(double *a, double *b);
	void setDirection(double a, double b);

	void getWidthHeight(int *a, int *b);

	void Draw(int tex_id);
	void DrawRect(int tex_id,float xo,float yo,float xf,float yf);

	void Move(int *map);
	bool Collides(int *map);
	bool CollidesStatic(int *map);
	void CollidesPlayer();

	void setState(int a);
	int getState();

	int getIdShooter();
	int getType();
	void setType(int t);
	int getDamage();
	void setDamage(int d);
};

