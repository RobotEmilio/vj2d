#pragma once

#include "cScene.h"
#include "cEnemy.h"
#include "cData.h"
#include "cMenu.h"
#include "cShoot.h"
#include "cBomb.h"
#include "Constants.h"
#include "Globals.h"
#include <map>
#include <string>
#include <vector>
#include <cstdlib>

using namespace std;

class cGame
{
public:
	//Creators
	cGame(void);
	virtual ~cGame(void);
	
	//Inits
	bool Init(int lvl, int estado);
	bool Loop();
	void Finalize();

	//Input
	void ReadKeyboard(unsigned char key, int x, int y, bool press);
	void ReadKeyboardSpecial(unsigned char key, int x, int y, bool press);
	void ReadMouse(int button, int state, int x, int y);

	//Process
	bool Process();

	//Output
	void Render();
	void BulletDamage(list<cShoot> * c, cPlayer * p);

	void ResetGame(int lvl, int estado);

	void switchGodMode();
	bool getGodMode();

	void InitializeEnemies();

private:
	//game variables
	unsigned char keys[256], sKeys[256];
	cScene Scene;
	cPlayer Player, Player2;
	list<cShoot> Bullets;
	list<cEnemy> Enemies;
	list<cBomb> Bombs;
	cData Data;
	cMenu Menu;

	//Nivel actual
	int currentlevel;

	//Estado del juego
	int state;

	//Activado multiplayer
	bool multiplayer;

	//Sprites pre-calculated coords
	map<char, maxmin> *dictionaryCoord;

	//game over
	float gOMovement;
	int gOPosition;
	//level change
	float lcMovement;

	bool godmode;
};
