
#include "cPlayer.h"

//Creator functions
cPlayer::cPlayer() {
	score = 0;
	bombs = 1;
	lives = 9;
	cameralock = false;
}
cPlayer::~cPlayer(){}

void cPlayer::Initialization() {
	SetWidthHeight(32,32);
	setScore(0);
	setLives(9);
	setBombs(1);
	ResetBombarounds();
}

void cPlayer::Initialization1() {
	SetTile(20,1);
	SetState(STATE_LOOKRIGHT);
	setId(1);
	Initialization();
}

void cPlayer::Initialization2() {
	SetTile(25,1);
	SetState(STATE_LOOKLEFT);
	setId(2);
	Initialization();
}

//Draw function
void cPlayer::Draw(int tex_id)
{	
	float xo,yo,xf,yf;
	float px, py; //Proporciones
	//X -> 32/192 = 0.167
	//Y -> 32/256 = 0.125
	px = 0.167f;
	py = 0.125f;

	switch(GetState())
	{
		
		//LOOKS
		case STATE_LOOKLEFT:		xo = 0.0f*px;	yo = 7.0f*py; break;
		case STATE_LOOKRIGHT:		xo = 0.0f*px;	yo = 3.0f*py; break;
		case STATE_LOOKTOP:			xo = 0.0f*px;	yo = 1.0f*py; break;
		case STATE_LOOKBOTTOM:		xo = 0.0f*px;	yo = 5.0f*py; break;
		case STATE_LOOKTOPLEFT:		xo = 0.0f*px;	yo = 8.0f*py; break;
		case STATE_LOOKTOPRIGHT:	xo = 0.0f*px;	yo = 2.0f*py; break;
		case STATE_LOOKBOTTOMLEFT:	xo = 0.0f*px;	yo = 6.0f*py; break;
		case STATE_LOOKBOTTOMRIGHT:	xo = 0.0f*px;	yo = 4.0f*py; break;
		
		//WALKS
		case STATE_WALKLEFT:		xo = 1.0f*px + (GetFrame()*px);	yo = 7.0f*py;
									NextFrame(4);
									break;
		
		case STATE_WALKRIGHT:		xo = 1.0f*px + (GetFrame()*px);	yo = 3.0f*py;
									NextFrame(4);
									break;

		case STATE_WALKTOP:			xo = 1.0f*px + (GetFrame()*px);	yo = 1.0f*py;
									NextFrame(4);
									break;

		case STATE_WALKBOTTOM:		xo = 1.0f*px + (GetFrame()*px);	yo = 5.0f*py;
									NextFrame(4);
									break;

		case STATE_WALKBOTTOMRIGHT:	xo = 1.0f*px + (GetFrame()*px);	yo = 4.0f*py;
									NextFrame(4);
									break;

		case STATE_WALKTOPRIGHT:	xo = 1.0f*px + (GetFrame()*px);	yo = 2.0f*py;
									NextFrame(4);
									break;

		case STATE_WALKTOPLEFT:		xo = 1.0f*px + (GetFrame()*px);	yo = 8.0f*py;
									NextFrame(4);
									break;

		case STATE_WALKBOTTOMLEFT:	xo = 1.0f*px + (GetFrame()*px);	yo = 6.0f*py;
									NextFrame(4);
									break;

	}
	xf = xo + px;
	yf = yo - py;

	DrawRect(tex_id,xo,yo,xf,yf);
}


//Score functions
void cPlayer::addScore(double s)
{
	this->score += s;
}
double cPlayer::getScore()
{
	return this->score;
}
void cPlayer::setScore(double s)
{
	this->score = s;
}


//Lives functions
void cPlayer::setLives(int l)
{
	this->lives = l;
}
int cPlayer::getLives()
{
	return this->lives;
}
void cPlayer::decreaseLives() {
	--lives;
}


//Bomb functions
void cPlayer::setBombs(int b)
{
	this->bombs = b;
}
int cPlayer::getBombs()
{
	return this->bombs;
}
bool cPlayer::CollideBombs(int playerx, int playery, int playerbombarounds) {
	if (playerbombarounds <= 0 || playerbombarounds >= 30) return false;
	playerx = playerx/TILE_SIZE;
	playery = playery/TILE_SIZE;
	if (x/TILE_SIZE < playerx-BOMB_RANGE || x/TILE_SIZE > playerx+BOMB_RANGE) return false;
	if ((y/TILE_SIZE - (playery+playerbombarounds)) > 0) return false;
	this->setLives(0);
	return true;
}

void cPlayer::readKeyboard(playerkeys kp1, int * map, list<cShoot> * bullets, unsigned char * keys, unsigned char * sKeys, list<cBomb> * bombas) {
	if (keys[kp1.left] && !keys[kp1.up] && !keys[kp1.right] && !keys[kp1.down])			MoveLeft(map); 
	else if (!keys[kp1.left] && !keys[kp1.up] && keys[kp1.right] && !keys[kp1.down])	MoveRight(map);
	else if (!keys[kp1.left] && keys[kp1.up] && !keys[kp1.right] && !keys[kp1.down])	MoveTop(map);
	else if (!keys[kp1.left] && !keys[kp1.up] && !keys[kp1.right] && keys[kp1.down])	MoveBottom(map);
	else if (keys[kp1.left] && !keys[kp1.up] && !keys[kp1.right] && keys[kp1.down])		MoveBottomLeft(map);
	else if (!keys[kp1.left] && !keys[kp1.up] && keys[kp1.right] && keys[kp1.down])		MoveBottomRight(map);
	else if (keys[kp1.left] && keys[kp1.up] && !keys[kp1.right] && !keys[kp1.down])		MoveTopLeft(map);
	else if (!keys[kp1.left] && keys[kp1.up] && keys[kp1.right] && !keys[kp1.down])		MoveTopRight(map);
	else Stop();
	
	
	if (((id == 1 && keys[kp1.shoot]) || (id == 2 && sKeys[kp1.shoot])) && !isShooting()) {
		cShoot aux(Shoot());
		bullets->push_back(aux);
	}
	else notShooting();
}

bool cPlayer::getCameraLock() {
	return cameralock;
}

void cPlayer::setCameraLock(int a) {
	if (y > a) {
		if ((y/TILE_SIZE)-(a/TILE_SIZE) >= 25) { 
			cameralock = true;
			Stop();
		}
		else cameralock = false;
	}
}

void cPlayer::Logic() {
	if (id > 0 && damage_invencibility > 0) --damage_invencibility;
}