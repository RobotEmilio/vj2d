#pragma once

#include "cBicho.h"
#include "Constants.h"
#include <string>

using namespace std;

class cPlayer: public cBicho
{
public:
	//Creadoras
	cPlayer();
	~cPlayer();

	//Inicializadoras
	void Initialization();
	void Initialization1();
	void Initialization2();

	//Metainformacion
	void addScore(double s);
	double getScore();
	void setScore(double s);

	void setLives(int l);
	int getLives();
	void decreaseLives();

	void setBombs(int b);
	int getBombs();

	//Player Logic
	void Draw(int tex_id);
	bool CollideBombs(int playerx, int playery, int playerbombarounds);
	void readKeyboard(playerkeys kp1, int * map, list<cShoot> * bullets, unsigned char * keys, unsigned char * sKeys, list<cBomb> * bombas);
	void Logic();
	bool getCameraLock();
	void setCameraLock(int a);

protected: 
	double score;
	int lives, bombs;
	bool cameralock;
};
