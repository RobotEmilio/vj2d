#include "cMenu.h"

cMenu::cMenu() {}
	
cMenu::~cMenu() {}

bool cMenu::init() 
{
	modify(MENU,1);
	
	return (true) ;
}

void cMenu::modify(int menID, int os) 
{
	if(menID == MENU || menID == MENU_ING) previousMenu = menID;
	menuID = menID;
	optionSelected = os;
}

int cMenu::returnPreviousMenu() 
{
	menuID = previousMenu;
	return menuID;
}

int cMenu::keyPress(int keyID) 
{
	switch (keyID)
	{
	case GLUT_KEY_UP:
		--optionSelected;
		if(optionSelected == 0 && menuID == MENU) optionSelected = MENU_NUM_EL;
		else if(optionSelected == 0 && menuID == MENU_ING) optionSelected = MENU_ING_NUM_EL;
		break;
	case GLUT_KEY_DOWN:
		++optionSelected;
		if(menuID == MENU && optionSelected > MENU_NUM_EL) optionSelected = 1;
		else if(menuID == MENU_ING && optionSelected > MENU_ING_NUM_EL) optionSelected = 1;
		break;
	case KEYBOARD_KEY_INTRO:
		if(menuID == MENU) {
			if(optionSelected == 1 ) return PLAY;
			else if(optionSelected == 2) return MULTIPLAYER;
			else if(optionSelected == 3) return HELP;
			else if(optionSelected == 4) return CREDITS;
			else if(optionSelected == 5 ) return EXIT;

		}
		else {
			if(optionSelected == 1 ) return PLAY;
			else if (optionSelected == 2) return HELP;
			else if( optionSelected == MENU_ING_NUM_EL) {
				modify(MENU,0);
				return EXIT_TO_MMENU;
			}
		}
	default:
		break;
	}
	return -1;
}

void cMenu::renderTitle(map<char, maxmin> *dictionaryCoord)
{
	//TITLE
	glBindTexture(GL_TEXTURE_2D,IMG_TITLE_TEXT);
	string title= "outchaos";
	if(menuID == MENU_ING) title = "paused"; 
	glBegin(GL_QUADS);
		int sumx = 0;
		for(char& c : title) {
			maxmin font = (*dictionaryCoord)[c];		
			glTexCoord2f(font.xmin, font.ymin);	
			glVertex2i(INI_TITLE_X+sumx+CHAR_SEPARATION_HOR,INI_TITLE_Y);
			glTexCoord2f(font.xmin, font.ymax);	
			glVertex2i(INI_TITLE_X+sumx+CHAR_SEPARATION_HOR,INI_TITLE_Y-TITLE_LETTER_HEIGHT);
			glTexCoord2f(font.xmax, font.ymax);	
			glVertex2i(INI_TITLE_X+TITLE_LETTER_WIDTH+sumx,INI_TITLE_Y-TITLE_LETTER_HEIGHT);
			glTexCoord2f(font.xmax,font.ymin);	
			glVertex2i(INI_TITLE_X+TITLE_LETTER_WIDTH+sumx,INI_TITLE_Y);
			sumx += TITLE_LETTER_WIDTH;
		}
	glEnd();
}

void cMenu::render(map<char, maxmin> *dictionaryCoord) 
{
	utils::renderBackground();
	glEnable(GL_TEXTURE_2D);
	
	//Title
	renderTitle(dictionaryCoord);

	//MENU
	glBindTexture(GL_TEXTURE_2D,IMG_MENU_TEXT);
	int sumy, numPar, sumx;
	sumx = sumy = 0;
	numPar = 1;
	string title;
	if(menuID == MENU ) title= "single<multiplayer<help<credits<exit";
	else if (menuID == MENU_ING) {
		title= "continue<help<exit to title";
		sumy += NORMAL_LETTER_HEIGHT + CHAR_SEPARATION_VERT;
	}
	glBindTexture(GL_TEXTURE_2D,IMG_MENU_TEXT);
	glBegin(GL_QUADS);
		for(char& c : title) {
			if(c == '<') {
				sumy += NORMAL_LETTER_HEIGHT + CHAR_SEPARATION_VERT;
				sumx = 0;
				numPar += 1;
			}
			else if(c == ' ') sumx += CHAR_SEPARATION_HOR + 2;
			else {
				maxmin font = (*dictionaryCoord)[c];	
				int inix, iniy, letterwidth,letterheigth;
				if(numPar == optionSelected) {
					inix = INI_MENU_X;
					iniy = INI_MENU_Y+5;
					letterwidth = SELECTED_LETTER_WIDTH;
					letterheigth = SELECTED_LETTER_HEIGHT;
				}
				else {
					inix = INI_MENU_X;
					iniy = INI_MENU_Y;
					letterwidth = NORMAL_LETTER_WIDTH;
					letterheigth = NORMAL_LETTER_HEIGHT;
				}
				glTexCoord2f(font.xmin, font.ymin);	
				glVertex2i(inix+sumx+CHAR_SEPARATION_HOR,iniy-sumy);
				glTexCoord2f(font.xmin, font.ymax);	
				glVertex2i(inix+sumx+CHAR_SEPARATION_HOR,iniy-letterheigth-sumy);
				glTexCoord2f(font.xmax, font.ymax);	
				glVertex2i(inix+letterwidth+sumx,iniy-letterheigth-sumy);
				glTexCoord2f(font.xmax,font.ymin);	
				glVertex2i(inix+letterwidth+sumx,iniy-sumy);
				sumx += letterwidth;
			}
		}
	glEnd();
}