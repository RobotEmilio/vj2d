#include "cBomb.h"


cBomb::cBomb(void)
{
	seq=0;
	delay=0;
	explosiontime=0;
}

cBomb::cBomb(int px, int py)
{
	seq=0;
	delay=0;
	explosiontime=0;
	x = px;
	y = py;
}

cBomb::~cBomb(void)
{
}

void cBomb::Draw(int tex_id) {
	float xo, yo, xf, yf;
	float bx, by; //Proporciones
	//320x80
	//x = 16/320 = 0.05
	//y = 16/80 = 0.2
	//textura de 20 x 4 tiles
	bx = 0.05; by = 0.2;
	xo = 0.0f*bx + (GetFrame()*bx); yo = 1.0f*by;
	NextFrame(10);
	xf = xo + bx;
	yf = yo - by;
	DrawRect(tex_id,xo,yo,xf,yf);
}

void cBomb::DrawRect(int tex_id,float xo,float yo,float xf,float yf) {
	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_id);

	glBegin(GL_QUADS);	
	for (int i = -1*BOMB_RANGE; i < (BOMB_RANGE); ++i) {
		int ax = (x+TILE_SIZE)+(i*BOMB_WIDTH);
		glTexCoord2f(xo,yo);	glVertex2i(ax  ,y);
		glTexCoord2f(xf,yo);	glVertex2i(ax+BOMB_WIDTH,y);
		glTexCoord2f(xf,yf);	glVertex2i(ax+BOMB_WIDTH,y+BOMB_WIDTH);
		glTexCoord2f(xo,yf);	glVertex2i(ax  ,y+BOMB_WIDTH);
	}
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void cBomb::NextFrame(int max)
{
	delay++;
	if(delay == FRAME_DELAY)
	{
		seq++;
		seq%=max;
		delay = 0;
	}
}
int cBomb::GetFrame()
{
	return seq;
}

bool cBomb::isEnded()
{
	if (explosiontime == BOMB_LASTS) return true;
	else {
		++explosiontime;
		return false;
	}
}