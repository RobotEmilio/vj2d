#include "cScene.h"
#include "Globals.h"

cScene::cScene(void)
{
}

cScene::~cScene(void)
{
}

bool cScene::LoadLevel(int level)
{
	bool res;
	FILE *fd, *cfd;
	char file[25];
	char file2[34];
	int i,j,px,py;
	float coordx_tile, coordy_tile;

	res=true;
	if(level<10) sprintf(file,"%s%s0%d%s",(char *)MAPROUTE,(char *)FILENAME,level,(char *)FILENAME_EXT);
	else		 sprintf(file,"%s%s%d%s",(char *)MAPROUTE,(char *)FILENAME,level,(char *)FILENAME_EXT);

	fd=fopen(file,"r");

	if(level<10) sprintf(file2,"%s%s0%d%s",(char *)MAPROUTE,(char *)STATICCOLLISION,level,(char *)FILENAME_EXT);
	else		 sprintf(file2,"%s%s%d%s",(char *)MAPROUTE,(char *)STATICCOLLISION,level,(char *)FILENAME_EXT);

	cfd=fopen(file2,"r");
	if(fd==NULL || cfd==NULL) return false;

	id_DL=glGenLists(1);
	glNewList(id_DL,GL_COMPILE);
		glBegin(GL_QUADS);
	
			int tile[1024];
			int collision[1024];
			for(j=SCENE_HEIGHT-1;j>=0;j--)
			{
				px=SCENE_Xo;
				py=SCENE_Yo+(j*TILE_SIZE);

				

				for(i=0;i<SCENE_WIDTH;i++)
				{
					fscanf(fd,"%i",tile);
					fscanf(cfd,"%i",collision);					

					float mx, my;
					int tilesxfila;

					///////////////////////
					//Mapa 1
					//432x304
					//x = 16/432 = 0,037037
					//y = 16/304 = 0,052631
					//27 tiles por fila
					///////////////////////
					//Mapa 2
					//x = 16/640 = 0.025
					//y = 16/608 = 0.02631
					//40 tiles por fila
					///////////////////////

					if (level == 1) {
						mx = 0.0370;
						my = 0.0526;
					}
					else if (level == 2) {
						mx = 0.025;
						my = 0.02631;
					}
						
					map[(j*SCENE_WIDTH)+i] = tile[0];
					if (collision[0] == 1) map[(j*SCENE_WIDTH)+i] = map[(j*SCENE_WIDTH)+i] * -1;

					//Lo que hace el tiled es guardartelo como un vector: es decir, el (0,0) lo guarda como 1,
					//el (1,0) lo guarda como 2, el (2,0) lo guarda como 3 y cuando llega al (0,1) es el 9
					//entonces el 33 -> (0,5) -> ((33-1)%8, (33-1)/8)
						if (level == 1) tilesxfila = 27;
						else if (level == 2) tilesxfila = 40;
						int x = (tile[0]-1)%tilesxfila;
						int y = (tile[0]-1)/tilesxfila;

						coordx_tile = x*mx;
						coordy_tile = y*my;

					//BLOCK_SIZE = 24, FILE_SIZE = 64
					// 24 / 64 = 0.375
					glTexCoord2f(coordx_tile		,coordy_tile+my	);	glVertex2i(px           ,py           );
					glTexCoord2f(coordx_tile+mx		,coordy_tile+my	);	glVertex2i(px+BLOCK_SIZE,py           );
					glTexCoord2f(coordx_tile+mx		,coordy_tile	);	glVertex2i(px+BLOCK_SIZE,py+BLOCK_SIZE);
					glTexCoord2f(coordx_tile		,coordy_tile	);	glVertex2i(px           ,py+BLOCK_SIZE);
					
					px+=TILE_SIZE;
				}
				fscanf(fd,"%i",tile); //pass enter
			}

		glEnd();
	glEndList();
	
	fclose(fd);
	return res;
}


int cScene::getOffset() {
	return offset;
}

void cScene::setOffset(int value) {
	if(value < 2918) { //end of the level sprite vision
		int diferencia = value - (SCENE_HEIGHT);
		if (diferencia > 0) offset = diferencia;
		else offset = 0;
	}
}

void cScene::Draw(int tex_id)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_id);
	glTranslatef(0.0f, -offset, 0.0f);
	glCallList(id_DL);
	glDisable(GL_TEXTURE_2D);
}

int* cScene::GetMap()
{
	return map;
}

bool cScene::detectCollision(int x, int y) {
	if (y < 15) return true; //Para que no aparezcan enemigos demasiado abajo
	if (map[y*SCENE_WIDTH + x] < 0) return true;
	if (map[y*SCENE_WIDTH + x+1] < 0) return true;
	if (map[y+1*SCENE_WIDTH + x] < 0) return true;
	if (map[y+1*SCENE_WIDTH + x+1] < 0) return true;
	return false;
}

bool cScene::checkFinal(int x, int y, int level) {
	x = x/TILE_SIZE;
	y = y/TILE_SIZE;
	return (x >= 18 && x <= 25) && (y >= 192) ;
}