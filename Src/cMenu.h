#pragma once

#include <string>
#include <map>
#include <array>
#include "cTexture.h"
#include "cData.h"
#include "Constants.h"
#include "Utils.h"

using namespace std;

class cMenu
{
public:
	cMenu(void);
	virtual ~cMenu(void);
	bool init();
	void render(map<char, maxmin> *dictionaryCoord);
	void modify(int menID, int os);
	int keyPress(int keyID);
	int returnPreviousMenu();
	void renderTitle(map<char, maxmin> *dictionaryCoord);
	int previousMenu;
private:
	int menuID, optionSelected;
};
