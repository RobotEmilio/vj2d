#include "cBicho.h"
#include "cScene.h"
#include "Globals.h"

//Creadoras
cBicho::cBicho(void)
{
	seq=0;
	delay=0;
	shootingdelay = GUN_DELAY;
	damage_invencibility = 0;
}
cBicho::cBicho(int posx,int posy,int width,int height)
{
	x = posx;
	y = posy;
	w = width;
	h = height;
	shootingdelay = GUN_DELAY;
	bombarounds = 0;
	damage_invencibility = 0;
}
cBicho::~cBicho(void){}

//Setters & Getters
void cBicho::SetPosition(int posx,int posy)
{
	x = posx;
	y = posy;
}
void cBicho::GetPosition(int *posx,int *posy)
{
	*posx = x;
	*posy = y;
}

void cBicho::SetTile(int tx,int ty)
{
	x = tx * TILE_SIZE;
	y = ty * TILE_SIZE;
}
void cBicho::GetTile(int *tx,int *ty)
{
	*tx = x / TILE_SIZE;
	*ty = y / TILE_SIZE;
}

void cBicho::SetWidthHeight(int width,int height)
{
	w = width;
	h = height;
}
void cBicho::GetWidthHeight(int *width,int *height)
{
	*width = w;
	*height = h;
}

int cBicho::getId() {
	return id;
}
void cBicho::setId(int i) {
	id = i;
}

//Collision functions
bool cBicho::Collides(int *map) {
	bool b = CollidesBoundaries(map) || CollidesStatic(map);
	return b;
}
bool cBicho::CollidesBoundaries(int *map) {
	if ((x < 0) || (x+w > (SCENE_WIDTH)*TILE_SIZE) || (y < 0) || (y+h > (SCENE_HEIGHT)*TILE_SIZE)) return true;
	return false;
}
bool cBicho::CollidesStatic(int *map) {
	//El -1 es de ajuste al poligono
	int xt = (x/TILE_SIZE);
	int yt = (y/TILE_SIZE);
	int wt = ((x+w-1)/TILE_SIZE);
	int ht = ((y+h-1)/TILE_SIZE);

	/*Tile intercalado*/
	if ((wt - xt) != 1) {
		if (map[xt+1 + yt*SCENE_WIDTH] < 0) return true;
		if (map[xt+1 + ht*SCENE_WIDTH] < 0) return true;
	}
	if ((ht - yt) != 1) {
		if (map[xt + (yt+1)*SCENE_WIDTH] < 0) return true;
		if (map[wt + (yt+1)*SCENE_WIDTH] < 0) return true;
	}

	/*Tile normal*/
	if (map[xt + yt*SCENE_WIDTH] < 0) return true;
	if (map[wt + ht*SCENE_WIDTH] < 0) return true;
	if (map[wt + yt*SCENE_WIDTH] < 0) return true;
	if (map[xt + ht*SCENE_WIDTH] < 0) return true;

	return false;
}

/*ACTIONS*/
//Movement functions
void cBicho::MoveLeft(int *map)
{
    x -= STEP_LENGTH;
    if(state != STATE_WALKLEFT)
    {
            state = STATE_WALKLEFT;
            seq = 0;
            delay = 0;
    }
	if (Collides(map)) {
		x += STEP_LENGTH;
		state = STATE_LOOKLEFT;
	}
}
void cBicho::MoveRight(int *map)
{
	x += STEP_LENGTH;

	if(state != STATE_WALKRIGHT)
	{
			state = STATE_WALKRIGHT;
			seq = 0;
			delay = 0;
	}
	if (Collides(map)) {
		x -= STEP_LENGTH;
		state = STATE_LOOKRIGHT;
	}
}
void cBicho::MoveTop(int *map)
{
    y += STEP_LENGTH;
    if(state != STATE_WALKTOP)
    {
            state = STATE_WALKTOP;
            seq = 0;
            delay = 0;
    }
	if (Collides(map)) {
		y -= STEP_LENGTH;
		state = STATE_LOOKTOP;
	}
}
void cBicho::MoveBottom(int *map)
{
    y -= STEP_LENGTH;
    if(state != STATE_WALKBOTTOM)
    {
            state = STATE_WALKBOTTOM;
            seq = 0;
            delay = 0;
    }
	if (Collides(map)) {
		y += STEP_LENGTH;
		state = STATE_LOOKBOTTOM;
	}
 
}
void cBicho::MoveBottomLeft(int *map)
{
	x -= STEP_LENGTH;
	y -= STEP_LENGTH;
	if(state != STATE_WALKBOTTOMLEFT)
	{
		state = STATE_WALKBOTTOMLEFT;
		seq = 0;
		delay = 0;
	}
	if (Collides(map)) {
		x += STEP_LENGTH;
		y += STEP_LENGTH;
		state = STATE_LOOKBOTTOMLEFT;
	}    
}
void cBicho::MoveBottomRight(int *map)
{
	x += STEP_LENGTH;
    y -= STEP_LENGTH;
    if(state != STATE_WALKBOTTOMRIGHT)
    {
            state = STATE_WALKBOTTOMRIGHT;
            seq = 0;
            delay = 0;
    }
	if (Collides(map)) {
		x -= STEP_LENGTH;
		y += STEP_LENGTH;
		state = STATE_LOOKBOTTOMRIGHT;
	} 
}
void cBicho::MoveTopLeft(int *map)
{
 	x -= STEP_LENGTH;
    y += STEP_LENGTH;
    if(state != STATE_WALKTOPLEFT)
    {
            state = STATE_WALKTOPLEFT;
            seq = 0;
            delay = 0;
    }
	if (Collides(map)) {
		x += STEP_LENGTH;
		y -= STEP_LENGTH;
		state = STATE_LOOKTOPLEFT;
	} 

}
void cBicho::MoveTopRight(int *map)
{
	x += STEP_LENGTH;
    y += STEP_LENGTH;
    if(state != STATE_WALKTOPRIGHT)
    {
            state = STATE_WALKTOPRIGHT;
            seq = 0;
            delay = 0;
    }
	if (Collides(map)) {
		x -= STEP_LENGTH;
		y -= STEP_LENGTH;
		state = STATE_LOOKTOPRIGHT;
	} 
}
void cBicho::Stop()
{
	switch(state)
	{
		case STATE_WALKLEFT:		state = STATE_LOOKLEFT;			break;
		case STATE_WALKRIGHT:		state = STATE_LOOKRIGHT;		break;
		case STATE_WALKTOP:			state = STATE_LOOKTOP;			break;
		case STATE_WALKBOTTOM:		state = STATE_LOOKBOTTOM;		break;
		case STATE_WALKBOTTOMLEFT:	state = STATE_LOOKBOTTOMLEFT;	break;
		case STATE_WALKBOTTOMRIGHT:	state = STATE_LOOKBOTTOMRIGHT;	break;
		case STATE_WALKTOPRIGHT:	state = STATE_LOOKTOPRIGHT;		break;
		case STATE_WALKTOPLEFT:		state = STATE_LOOKTOPLEFT;		break;
	}
}

//Shooting functions
cShoot cBicho::Shoot() {
	shootingdelay = GUN_DELAY;
	cShoot disparo(id,1,state,x,y);
	shooting = true;
	return disparo;
}

void cBicho::ShootBomb(list<cBomb> * Bombs) {	
	bombarounds++;
	cBomb bomba(x,y+(bombarounds*TILE_SIZE));
	Bombs->push_back(bomba);
}

/*Logic functions*/

//Shooting logic
bool cBicho::isShot(int idbullet, int bx, int by, int bw, int bh) {
	if (id > 0 && damage_invencibility > 0) return false;
	if (idbullet > 0 && id > 0) return false;	//Los heroes no se hacen da�o entre ellos
	if (idbullet < 0 && id < 0) return false;	//Los enemigos no se hacen da�o entre ellos
	if ((x <= bx+bw && x+w >= bx+bw) || (x+w >= bx && x < bx)) {
		if ((y <= by+bh && y+h >= by+bh) || (y+h >= by && y <= by) || (y <= by && y+h >= by+bh)) {
			damage_invencibility = 40;
			return true;
		}
	}
	return false;
}
bool cBicho::isShooting() {
	return shooting;
}
void cBicho::notShooting() {
	--shootingdelay;
	if (shootingdelay <= 0)	shooting = false;
}
int cBicho::getBombarounds() {
	return bombarounds;
}
void cBicho::ResetBombarounds() {
	this->bombarounds = 0;
}

//Drawing functions
void cBicho::DrawRect(int tex_id,float xo,float yo,float xf,float yf)
{
	if (!(damage_invencibility > 0 && damage_invencibility%2 != 0)) {
		int screen_x,screen_y;

		screen_x = x + SCENE_Xo;
		screen_y = y + SCENE_Yo + (BLOCK_SIZE - TILE_SIZE);
	
		glEnable(GL_TEXTURE_2D);
	
		glBindTexture(GL_TEXTURE_2D,tex_id);
		glBegin(GL_QUADS);	
			glTexCoord2f(xo,yo);	glVertex2i(screen_x  ,screen_y);
			glTexCoord2f(xf,yo);	glVertex2i(screen_x+w,screen_y);
			glTexCoord2f(xf,yf);	glVertex2i(screen_x+w,screen_y+h);
			glTexCoord2f(xo,yf);	glVertex2i(screen_x  ,screen_y+h);
		glEnd();

		glDisable(GL_TEXTURE_2D);
	}
}
void cBicho::NextFrame(int max)
{
	delay++;
	if(delay == FRAME_DELAY)
	{
		seq++;
		seq%=max;
		delay = 0;
	}
}
int cBicho::GetFrame()
{
	return seq;
}
int cBicho::GetState()
{
	return state;
}
void cBicho::SetState(int s)
{
	state = s;
}
void cBicho::GetArea(cRect *rc) {
	rc->left   = x;
	rc->right  = x+w;
	rc->bottom = y;
	rc->top    = y+h;
}
