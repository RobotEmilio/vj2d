#include "cShoot.h"

cShoot::cShoot(void)
{
	hit = false;
	w = h = 8;
}

cShoot::cShoot(cShoot * c)
{
	hit = false;

	idshooter = c->getIdShooter();
	c->getPosition(&x,&y);
	c->getWidthHeight(&w, &h);
	c->getDirection(&dx, &dy);
	distance = c->getDistance();
	state = c->getState();

}

cShoot::cShoot(int shooter,int type, int estado, int a, int b) {
	idshooter = shooter;
	hit = false;
	this->type = type;
	if (type == BULLET) damage = DBULLET;

	distance = 40;
	x = a+(TILE_SIZE);
	y = b+(TILE_SIZE);
	w = h = 8;

												//Los valores sumados a x e y son para mejorar la animación de donde surgen las balas
	if (estado < 3)								{state = STATE_BLEFT; dx = -1; dy = 0; x-=(TILE_SIZE);}
	else if (estado >= 3 && estado < 6)			{state = STATE_BTOPLEFT; dx = -1; dy = 1; x-=(TILE_SIZE); y+=(TILE_SIZE);}// y-=4; x-=4;}
	else if (estado >= 6 && estado < 9)			{state = STATE_BTOP; dx = 0; dy = 1; y+=(TILE_SIZE);}
	else if (estado >= 9 && estado < 12)		{state = STATE_BTOPRIGHT; dx = 1; dy = 1; x+=(TILE_SIZE); y+=(TILE_SIZE);}
	else if (estado >= 12 && estado < 15)		{state = STATE_BRIGHT; dx = 1; dy = 0; x+=(TILE_SIZE/2); y-=(TILE_SIZE/2);}// y-=4;}
	else if (estado >= 15 && estado < 18)		{state = STATE_BBOTTOMRIGHT; dx = 1; dy = -1; x+=(TILE_SIZE/2); y-=(TILE_SIZE);}// y-=4; x-=4;}
	else if (estado >= 18 && estado < 21)		{state = STATE_BBOTTOM; dx = 0; dy = -1; x-=(TILE_SIZE/2); y-=(TILE_SIZE);}// x-=4;}
	else if (estado >= 21 && estado < 24)		{state = STATE_BBOTTOMLEFT; dx = -1; dy = -1; x-=(TILE_SIZE); y-=(TILE_SIZE);}
}

cShoot::~cShoot(void)
{
}

int cShoot::getDistance() {
	return distance;
}
void cShoot::setDistance(int a) {
	distance = a;
}

void cShoot::getPosition(int *a, int *b) {
	*a = x;
	*b = y;
}
void cShoot::setPosition(int a, int b) {
	x = a;
	y = b;
}
void cShoot::setDirection(double a, double b) {
	dx = a;
	dy = b;
}
void cShoot::getDirection(double *a, double *b) {
	*a = dx;
	*b = dy;
}
void cShoot::getWidthHeight(int *a, int *b) {
	*a = w;
	*b = h;
}

void cShoot::Draw(int tex_id) {
	float xo, yo, xf, yf;
	float bx, by; //Proporciones
	bx = 0.125f;
	by = 0.04762f;
	if (idshooter > 0){	
		if (state != STATE_BDESTROYED){ xo = 6.0f*bx;	yo = 1.0f*by;}
		else {xo = 4.0f*bx;	yo = 4.0f*by;}
	}
	else{				
		if (state != STATE_BDESTROYED){	xo = 1.0f*bx;	yo = 1.0f*by;}
		else {xo = 4.0f*bx;	yo = 4.0f*by;}
	}
	xf = xo + bx;
	yf = yo - by;

	DrawRect(tex_id,xo,yo,xf,yf);
}

void cShoot::DrawRect(int tex_id,float xo,float yo,float xf,float yf) {
	

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	
	glBindTexture(GL_TEXTURE_2D,tex_id);
	glBegin(GL_QUADS);	
		glTexCoord2f(xo,yo);	glVertex2i(x  ,y);
		glTexCoord2f(xf,yo);	glVertex2i(x+w,y);
		glTexCoord2f(xf,yf);	glVertex2i(x+w,y+h);
		glTexCoord2f(xo,yf);	glVertex2i(x  ,y+h);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void cShoot::Move(int *map) {
	x = x + dx*SHOOT_SPEED;
	y = y + dy*SHOOT_SPEED;
	--distance;
	if (Collides(map)) {
		x = x - dx*SHOOT_SPEED;
		y = y - dy*SHOOT_SPEED;
		dx = 0;
		dy = 0;
		state = STATE_BDESTROYED;
	}
}

bool cShoot::Collides(int *map) {
	return CollidesStatic(map) || hit;
}

bool cShoot::CollidesStatic(int *map) {
	//El -1 es de ajuste al poligono
	int xt = (x/TILE_SIZE);
	int yt = (y/TILE_SIZE);
	int wt = ((x+w)/TILE_SIZE);
	int ht = ((y+h)/TILE_SIZE);

	/*Tile intercalado*/
	if ((wt - xt) != 1) {
		if (map[xt+1 + yt*SCENE_WIDTH] < 0) return true;
		if (map[xt+1 + ht*SCENE_WIDTH] < 0) return true;
	}
	if ((ht - yt) != 1) {
		if (map[xt + (yt+1)*SCENE_WIDTH] < 0) return true;
		if (map[wt + (yt+1)*SCENE_WIDTH] < 0) return true;
	}

	/*Tile normal*/
	if (map[xt + yt*SCENE_WIDTH] < 0) return true;
	if (map[wt + ht*SCENE_WIDTH] < 0) return true;
	if (map[wt + yt*SCENE_WIDTH] < 0) return true;
	if (map[xt + ht*SCENE_WIDTH] < 0) return true;

	return false;
}

void cShoot::setState(int a) {
	state = a;
}

int cShoot::getState() {
	return state;
}

void cShoot::CollidesPlayer() {
	this->hit = true;
	state = STATE_BDESTROYED;
}

int cShoot::getIdShooter() {
	return idshooter;
}

int cShoot::getType() {
	return type;
}

void cShoot::setType(int t) {
	type = t;
}

int cShoot::getDamage() {
	return damage;
}

void cShoot::setDamage(int d) {
	damage = d;
}