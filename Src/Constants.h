
//GAME CONSTANTS
#define PLAY				0
#define MENU				1
#define MENU_ING			2
#define MULTIPLAYER			3
#define EXIT				4
#define HELP				5
#define CREDITS				6
#define GAME_OVER			7
#define EXIT_TO_MMENU		8
#define CHANGING_LEVEL		9

#define GAME_WIDTH			640
#define GAME_HEIGHT			480
#define GAME_SCENE_HEIGHT	2918
#define GAME_SCENE_WIDTH	480
#define GAME_FPS			15
#define SHOOT_SPEED			6

//NUM ELEMENTS FOR MENU
#define MENU_NUM_EL			5
#define MENU_ING_NUM_EL		3

//COORDS 
#define INI_MENU_X					150
#define INI_MENU_Y					260
#define NORMAL_LETTER_HEIGHT		28
#define NORMAL_LETTER_WIDTH			30
#define SELECTED_LETTER_HEIGHT		38
#define SELECTED_LETTER_WIDTH		40
#define INI_TITLE_X					100
#define INI_TITLE_Y					450
#define TITLE_LETTER_HEIGHT			150
#define TITLE_LETTER_WIDTH			60
#define CHAR_SEPARATION_HOR			5
#define CHAR_SEPARATION_VERT		25
#define INI_TITLE_GO_X				50
#define INI_TITLE_GO_Y				320
#define SUBTITLE_LETTER_W			25
#define SUBTITLE_LETTER_H			23
#define INI_SUBTITLE_GO_X			30
#define INI_SUBTITLE_GO_Y			90

//CUSTOM KEYS
#define KEYBOARD_KEY_INTRO			13
#define KEYBOARD_KEY_ESC			27

//Image array size
#define NUM_IMG		15

//Image identifiers
#define IMG_BLOCKS		0
#define IMG_PLAYER		1
#define IMG_TITLE_TEXT	3
#define IMG_MENU_TEXT	4
#define IMG_HELP		5
#define IMG_BACKGROUND	6
#define IMG_CREDITS		7
#define IMG_BULLETS1	8
#define IMG_SCORE		9
#define IMG_BOMB_INFO	10
#define IMG_ENEMY1		11
#define IMG_ENEMY2		12
#define IMG_PLAYER2		13
#define	IMG_BOMB		14

//Scene
#define SCENE_Xo		0
#define SCENE_Yo		0
#define SCENE_WIDTH		40
#define SCENE_HEIGHT	200
#define MAPROUTE		"Resources/Maps/"
#define FILENAME		"Lvl"
#define STATICCOLLISION "CollisionLvl"
#define FILENAME_EXT	".txt"
#define TILE_SIZE		16
#define BLOCK_SIZE		16

//PLAYER INFO
#define INI_Y_INFO_PLAYER	475
#define INI_X_INFO_P1		45
#define INI_X_INFO_P2		490
#define INI_X_INFO_L_P1		5
#define INI_X_INFO_L_P2		450
#define INFO_LETTER_WIDTH	20
#define INFO_LETTER_HEIGHT	18
#define INFO_BOMBS_Y		45
#define INFO_BOMBS_X		10
#define INFO_BOMBS_X_P2		570
#define INFO_BOMBS_WIDTH	30

//BICHO STATES
//Estados de caminar -> estados mod 3 == 0
#define STATE_WALKLEFT			0
#define STATE_WALKTOPLEFT		3
#define STATE_WALKTOP			6
#define STATE_WALKTOPRIGHT		9
#define STATE_WALKRIGHT			12
#define STATE_WALKBOTTOMRIGHT	15
#define STATE_WALKBOTTOM		18
#define STATE_WALKBOTTOMLEFT	21

//Estados de mirar -> estados mod 3 == 1
#define STATE_LOOKLEFT			1
#define STATE_LOOKTOPLEFT		4
#define STATE_LOOKTOP			7
#define STATE_LOOKTOPRIGHT		10
#define STATE_LOOKRIGHT			13
#define STATE_LOOKBOTTOMRIGHT	16
#define STATE_LOOKBOTTOM		19
#define STATE_LOOKBOTTOMLEFT	22

//Estados disparar -> estados mod 3 == 2
#define STATE_SHOOTLEFT			2
#define STATE_SHOOTTOPLEFT		5
#define STATE_SHOOTTOP			8
#define STATE_SHOOTTOPRIGHT		11
#define STATE_SHOOTRIGHT		14
#define STATE_SHOOTBOTTOMRIGHT	17
#define STATE_SHOOTBOTTOM		20
#define STATE_SHOOTBOTTOMLEFT	23

//Bicho Metadata
#define	GUN_DELAY	10
#define ENEMY_GUN_DELAY 20
#define ENEMY_SNIPER_DELAY 40
#define FRAME_DELAY		8
#define STEP_LENGTH		2

//ENEMIES
#define ENEMY_WIDTH			32

//BOMBS
#define BOMB_WIDTH		16
#define BOMB_RANGE		2
#define BOMB_LASTS		80  //FRAME_DELAY * FRAMES

//BULLET STATES
#define STATE_BTOP			1
#define STATE_BTOPLEFT		2
#define STATE_BLEFT			3
#define STATE_BBOTTOMLEFT	4
#define STATE_BBOTTOM		5
#define STATE_BBOTTOMRIGHT	6
#define STATE_BRIGHT		7
#define	STATE_BTOPRIGHT		8
#define STATE_BDESTROYED	9