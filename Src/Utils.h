
#include <map>
#include <string>
#include "Globals.h"
#include "Constants.h"

#include <iostream>
#include <sstream>
#include <string>
using namespace std;

struct maxmin {
	float xmin,ymin,xmax,ymax;
};

static class  utils
{
public:
	 utils();
	~ utils();
	static void calculateDictionaryTextBoundaries(int rows, int cols, map<char, maxmin> *dictMap);
	static void renderHelp();
	static void renderBackground();
	static void renderCredits();
	static void renderPlayerInfo(int player, int score, int lives, int bombs, map<char, maxmin> *dictMap, int offset);
	static void renderGameOver(string fixedLetters, char movementLetter,float movement, map<char, maxmin> *dictMap);
	static void renderLevelComplete(int level, double score, map<char, maxmin> *dictMap, float trans);
private:
	void static paintBackgroundQuad();
	void static paintSubtitle(string subtitle, map<char, maxmin> *dictMap, int textureID);
	int static paintFixLetters(string fixedLetters, map<char, maxmin> *dictMap, int textureID);
	void static paintMovementLetter(char movementLetter, float movement, int sumx,map<char, maxmin> *dictMap, int textureID);
};

