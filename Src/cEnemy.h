#pragma once
#include "cPlayer.h"
#include "Constants.h"
#include "Globals.h"

#define PI 3.1415926

class cEnemy :
	public cPlayer
{
public:
	cEnemy(void);
	cEnemy(int posx,int posy,int width,int height, int type);
	~cEnemy(void);

	int AiWalk();
	void setAiState(int state);

	void EnemyMove(int playerx, int playery, int * map);

	void EnemyAI(int *map, int playerx, int playery, list<cShoot> * l);
	void EnemyAI1(int *map, int playerx, int playery, list<cShoot> * l);
	void EnemyAI2(int *map, int playerx, int playery, list<cShoot> * l);

	cShoot SniperShoot(int playerx, int playery, double angulodisparo);
	cShoot Shoot();

	int getKind();
private:
	int iawalking;
	int iastate;
	int kind;
};

