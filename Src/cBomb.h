#pragma once

#include "Globals.h"
#include "Constants.h"

#define FRAME_DELAY		8


class cBomb
{
public:
	cBomb(void);
	cBomb(int px, int py);
	~cBomb(void);
	void Draw(int tex_id);
	void DrawRect(int tex_id,float xo,float yo,float xf,float yf);
	bool isEnded();

private:
	int x,y;
	int seq;
	int delay;
	int explosiontime;

	void NextFrame(int max);
	int  GetFrame();
};

