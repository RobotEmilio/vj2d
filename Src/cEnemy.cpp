#include "cEnemy.h"


cEnemy::cEnemy(void)
{
	iastate = -1;
}

cEnemy::cEnemy(int posx,int posy,int width,int height,int type)
{
	x = posx;
	y = posy;
	w = width;
	h = height;
	shootingdelay = GUN_DELAY;
	bombarounds = 0;
	iastate = -1;
	kind = type;
	lives = 3;
}

cEnemy::~cEnemy(void)
{
}


void cEnemy::EnemyAI(int *map, int playerx, int playery, list<cShoot> * l) {
	if (kind == 1) EnemyAI1(map, playerx, playery, l);
	else if (kind == 2) EnemyAI2(map, playerx, playery, l);
}
void cEnemy::EnemyAI1(int *map, int playerx, int playery, list<cShoot> * l)
{
	int distancia = sqrt(((x-playerx)*(x-playerx)) + ((y-playery)*(y-playery)));
	if (distancia < 200) {
		int a = rand()%5;	//un 20% de las veces se movera y un 80% de las veces disparará
		if (a <= 0) EnemyMove(playerx, playery, map);
		else {
			if (!isShooting()) l->push_back(Shoot());
			else notShooting();
		}
	}
	else {
		int a = AiWalk();
		if (a <= 0) {
			a = rand()%100;  //un 92% de las veces escogera pararse
		}
		switch(a) {
			case 0: {MoveBottom(map);		setAiState(0);	break;}
			case 1: {MoveTop(map);			setAiState(1);	break;}
			case 2:	{MoveLeft(map);			setAiState(2);	break;}
			case 3: {MoveRight(map);		setAiState(3);	break;}
			case 4: {MoveTopRight(map);		setAiState(4);	break;}
			case 5: {MoveTopLeft(map);		setAiState(5);	break;}
			case 6: {MoveBottomLeft(map);	setAiState(6);	break;}
			case 7: {MoveBottomRight(map);	setAiState(7);	break;}
			default: {Stop();				setAiState(-1);	break;}
		}
	}	
}
void cEnemy::EnemyAI2(int *map, int playerx, int playery, list<cShoot> * l)
{
	int distancia = sqrt(((x-playerx)*(x-playerx)) + ((y-playery)*(y-playery)));
	if (distancia < 350) {
		double angulodisparo = atan2((playerx-x),(playery-y));

		if (!isShooting()) l->push_back(SniperShoot(playerx, playery, angulodisparo));
		else notShooting();
		
		if (angulodisparo >= -PI/8 && angulodisparo < PI/8) state = STATE_LOOKTOP;
		else if (angulodisparo <= -PI/8 && angulodisparo > -3*PI/8)	state = STATE_LOOKTOPLEFT;
		else if (angulodisparo <= -3*PI/8 && angulodisparo > -5*PI/8)	state = STATE_LOOKLEFT;
		else if (angulodisparo <= -5*PI/8 && angulodisparo > -7*PI/8)	state = STATE_LOOKBOTTOMLEFT;
		else if (angulodisparo >= PI/8 && angulodisparo < 3*PI/8)	state = STATE_LOOKTOPRIGHT;
		else if (angulodisparo >= 3*PI/8 && angulodisparo < 5*PI/8)	state = STATE_LOOKRIGHT;
		else if (angulodisparo >= 5*PI/8 && angulodisparo < 7*PI/8)	state = STATE_LOOKBOTTOMRIGHT;
		else state = STATE_LOOKBOTTOM;
	}
}
void cEnemy::EnemyMove(int playerx, int playery, int * map) {
	if (x < playerx) {
		if (y < playery)				MoveTopRight(map);
		else if (y > playery)			MoveBottomRight(map);
		else							MoveRight(map);
		}
	else if (x > playerx) {
		if (y < playery)				MoveTopLeft(map);
		else if (y > playery)			MoveBottomLeft(map);
		else							MoveLeft(map);
	}
	else {
		if (y < playery)				MoveTop(map);
		else if (y > playery)			MoveBottom(map);
	}
}
int cEnemy::AiWalk() {
	if (iastate == -1) return -1;
	if (iawalking <= 0) {
		iawalking = 6;
		return -1;
	}
	else {
		--iawalking;
		return iastate;
	}
}
void cEnemy::setAiState(int state) {
	iastate = state;
}

int cEnemy::getKind() {
	return kind;
}

cShoot cEnemy::Shoot() {
	shootingdelay = ENEMY_GUN_DELAY;
	cShoot disparo(id,1,state,x,y);
	shooting = true;
	return disparo;
}

cShoot cEnemy::SniperShoot(int playerx, int playery, double angulodisparo) {
	shootingdelay = ENEMY_SNIPER_DELAY;
	cShoot disparo(id,1,state,x,y);
	double dirx = sin(angulodisparo);
	double diry = cos(angulodisparo);
	disparo.setDirection(dirx, diry);
	disparo.setDistance(60);
	shooting = true;
	return disparo;
}